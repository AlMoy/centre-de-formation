<?php

namespace App\EventSubscriber;

use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\SessionRepository;
use App\Entity\Session;


class CalendarSubscriber extends AbstractController implements EventSubscriberInterface
    {
    public static function getSubscribedEvents()
        {
        return [CalendarEvents::SET_DATA => 'onCalendarSetData',];
        }

    public function load(){}

    public function onCalendarSetData(CalendarEvent $calendar)
        {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

        foreach($this->getDoctrine()->getRepository(Session::class)->listSessionByMonth($start,$end) as $session)
            {
            $event=new Event(
                $session['nom'],
                $session['date_debut'],
                $session['date_fin']
                );
            $event->addOption('url',$this->generateUrl('session_show',['id' => $session['id']]));
            $calendar->addEvent($event);
            }
        }
    }
