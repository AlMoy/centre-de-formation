<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NiveauScolaireRepository")
 */
class NiveauScolaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $derniere_classe;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_derniere_classe;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dernier_diplome;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_dernier_diplome;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dernier_emploi;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_dernier_emploi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nom_dernier_employeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse_dernier_employeur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDerniereClasse(): ?string
    {
        return $this->derniere_classe;
    }

    public function setDerniereClasse(?string $derniere_classe): self
    {
        $this->derniere_classe = $derniere_classe;

        return $this;
    }

    public function getDateDerniereClasse(): ?\DateTimeInterface
    {
        return $this->date_derniere_classe;
    }

    public function setDateDerniereClasse(?\DateTimeInterface $date_derniere_classe): self
    {
        $this->date_derniere_classe = $date_derniere_classe;

        return $this;
    }

    public function getDernierDiplome(): ?string
    {
        return $this->dernier_diplome;
    }

    public function setDernierDiplome(?string $dernier_diplome): self
    {
        $this->dernier_diplome = $dernier_diplome;

        return $this;
    }

    public function getDateDernierDiplome(): ?\DateTimeInterface
    {
        return $this->date_dernier_diplome;
    }

    public function setDateDernierDiplome(?\DateTimeInterface $date_dernier_diplome): self
    {
        $this->date_dernier_diplome = $date_dernier_diplome;

        return $this;
    }

    public function getDernierEmploi(): ?string
    {
        return $this->dernier_emploi;
    }

    public function setDernierEmploi(?string $dernier_emploi): self
    {
        $this->dernier_emploi = $dernier_emploi;

        return $this;
    }

    public function getDateDernierEmploi(): ?\DateTimeInterface
    {
        return $this->date_dernier_emploi;
    }

    public function setDateDernierEmploi(?\DateTimeInterface $date_dernier_emploi): self
    {
        $this->date_dernier_emploi = $date_dernier_emploi;

        return $this;
    }

    public function getNomDernierEmployeur(): ?string
    {
        return $this->nom_dernier_employeur;
    }

    public function setNomDernierEmployeur(?string $nom_dernier_employeur): self
    {
        $this->nom_dernier_employeur = $nom_dernier_employeur;

        return $this;
    }

    public function getAdresseDernierEmployeur(): ?string
    {
        return $this->adresse_dernier_employeur;
    }

    public function setAdresseDernierEmployeur(?string $adresse_dernier_employeur): self
    {
        $this->adresse_dernier_employeur = $adresse_dernier_employeur;

        return $this;
    }
}
