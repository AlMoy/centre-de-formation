<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SuiviRepository")
 */
class Suivi
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact", inversedBy="suivis")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_contact;

    /**
     * @ORM\Column(type="date")
     */
    private $date_creation;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeSuivi")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_type;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Rappel", mappedBy="id_suivi", cascade={"persist", "remove"})
     */
    private $rappel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdContact(): ?Contact
    {
        return $this->id_contact;
    }

    public function setIdContact(?Contact $id_contact): self
    {
        $this->id_contact = $id_contact;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIdType(): ?TypeSuivi
    {
        return $this->id_type;
    }

    public function setIdType(?TypeSuivi $id_type): self
    {
        $this->id_type = $id_type;

        return $this;
    }

    public function getRappel(): ?Rappel
    {
        return $this->rappel;
    }

    public function setRappel(Rappel $rappel): self
    {
        $this->rappel = $rappel;

        // set the owning side of the relation if necessary
        if ($this !== $rappel->getIdSuivi()) {
            $rappel->setIdSuivi($this);
        }

        return $this;
    }
}
