<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DemandeurEmploiRepository")
 */
class DemandeurEmploi
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville_pole_emploi;

    /**
     * @ORM\Column(type="integer")
     */
    private $date_inscription;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $identifiant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_conseiller;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $antenne_mission_locale;

    /**
     * @ORM\Column(type="boolean")
     */
    private $CIVIS;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Allocation")
     */
    private $id_allocation;

    public function __construct()
    {
        $this->id_allocation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVillePoleEmploi(): ?string
    {
        return $this->ville_pole_emploi;
    }

    public function setVillePoleEmploi(string $ville_pole_emploi): self
    {
        $this->ville_pole_emploi = $ville_pole_emploi;

        return $this;
    }

    public function getDateInscription(): ?int
    {
        return $this->date_inscription;
    }

    public function setDateInscription(int $date_inscription): self
    {
        $this->date_inscription = $date_inscription;

        return $this;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(string $identifiant): self
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getNomConseiller(): ?string
    {
        return $this->nom_conseiller;
    }

    public function setNomConseiller(string $nom_conseiller): self
    {
        $this->nom_conseiller = $nom_conseiller;

        return $this;
    }

    public function getAntenneMissionLocale(): ?string
    {
        return $this->antenne_mission_locale;
    }

    public function setAntenneMissionLocale(string $antenne_mission_locale): self
    {
        $this->antenne_mission_locale = $antenne_mission_locale;

        return $this;
    }

    public function getCIVIS(): ?bool
    {
        return $this->CIVIS;
    }

    public function setCIVIS(bool $CIVIS): self
    {
        $this->CIVIS = $CIVIS;

        return $this;
    }

    /**
     * @return Collection|Allocation[]
     */
    public function getIdAllocation(): Collection
    {
        return $this->id_allocation;
    }

    public function addIdAllocation(Allocation $idAllocation): self
    {
        if (!$this->id_allocation->contains($idAllocation)) {
            $this->id_allocation[] = $idAllocation;
        }

        return $this;
    }

    public function removeIdAllocation(Allocation $idAllocation): self
    {
        if ($this->id_allocation->contains($idAllocation)) {
            $this->id_allocation->removeElement($idAllocation);
        }

        return $this;
    }
}
