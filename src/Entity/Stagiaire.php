<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StagiaireRepository")
 */
class Stagiaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $telephone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="date")
     */
    private $date_naissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lieu_naissance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nationalite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numero_secu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $situation_familiale;

    /**
     * @ORM\Column(type="boolean")
     */
    private $permis;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vehicule;

    /**
     * @ORM\Column(type="boolean")
     */
    private $travailleur_handicape;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DemandeurEmploi", cascade={"persist", "remove"})
     */
    private $id_demandeur_emploi;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\NiveauScolaire", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_niveau_scolaire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Statut")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_statut;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="stagiaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_entreprise;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ville", inversedBy="stagiaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_ville;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Document", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_document;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $num_rue;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rue;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Session", mappedBy="stagiaires")
     */
    private $sessions;

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->date_naissance;
    }

    public function setDateNaissance(\DateTimeInterface $date_naissance): self
    {
        $this->date_naissance = $date_naissance;

        return $this;
    }

    public function getLieuNaissance(): ?string
    {
        return $this->lieu_naissance;
    }

    public function setLieuNaissance(string $lieu_naissance): self
    {
        $this->lieu_naissance = $lieu_naissance;

        return $this;
    }

    public function getNationalite(): ?string
    {
        return $this->nationalite;
    }

    public function setNationalite(string $nationalite): self
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    public function getNumeroSecu(): ?string
    {
        return $this->numero_secu;
    }

    public function setNumeroSecu(string $numero_secu): self
    {
        $this->numero_secu = $numero_secu;

        return $this;
    }

    public function getSituationFamiliale(): ?string
    {
        return $this->situation_familiale;
    }

    public function setSituationFamiliale(string $situation_familiale): self
    {
        $this->situation_familiale = $situation_familiale;

        return $this;
    }

    public function getPermis(): ?bool
    {
        return $this->permis;
    }

    public function setPermis(bool $permis): self
    {
        $this->permis = $permis;

        return $this;
    }

    public function getVehicule(): ?bool
    {
        return $this->vehicule;
    }

    public function setVehicule(bool $vehicule): self
    {
        $this->vehicule = $vehicule;

        return $this;
    }

    public function getTravailleurHandicape(): ?bool
    {
        return $this->travailleur_handicape;
    }

    public function setTravailleurHandicape(bool $travailleur_handicape): self
    {
        $this->travailleur_handicape = $travailleur_handicape;

        return $this;
    }

    public function getIdDemandeurEmploi(): ?DemandeurEmploi
    {
        return $this->id_demandeur_emploi;
    }

    public function setIdDemandeurEmploi(?DemandeurEmploi $id_demandeur_emploi): self
    {
        $this->id_demandeur_emploi = $id_demandeur_emploi;

        return $this;
    }

    public function getIdNiveauScolaire(): ?NiveauScolaire
    {
        return $this->id_niveau_scolaire;
    }

    public function setIdNiveauScolaire(NiveauScolaire $id_niveau_scolaire): self
    {
        $this->id_niveau_scolaire = $id_niveau_scolaire;

        return $this;
    }

    public function getIdStatut(): ?Statut
    {
        return $this->id_statut;
    }

    public function setIdStatut(?Statut $id_statut): self
    {
        $this->id_statut = $id_statut;

        return $this;
    }

    public function getIdEntreprise(): ?Entreprise
    {
        return $this->id_entreprise;
    }

    public function setIdEntreprise(?Entreprise $id_entreprise): self
    {
        $this->id_entreprise = $id_entreprise;

        return $this;
    }

    public function getIdVille(): ?Ville
    {
        return $this->id_ville;
    }

    public function setIdVille(?Ville $id_ville): self
    {
        $this->id_ville = $id_ville;

        return $this;
    }

    public function getIdDocument(): ?Document
    {
        return $this->id_document;
    }

    public function setIdDocument(Document $id_document): self
    {
        $this->id_document = $id_document;

        return $this;
    }

    public function getNumRue(): ?string
    {
        return $this->num_rue;
    }

    public function setNumRue(string $num_rue): self
    {
        $this->num_rue = $num_rue;

        return $this;
    }

    public function getRue(): ?string
    {
        return $this->rue;
    }

    public function setRue(string $rue): self
    {
        $this->rue = $rue;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->addStagiaire($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
            $session->removeStagiaire($this);
        }

        return $this;
    }
}
