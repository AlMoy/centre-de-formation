<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RappelRepository")
 */
class Rappel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_rappel;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $valide;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Suivi", inversedBy="rappel")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_suivi;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Contact", inversedBy="rappels")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_contact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateRappel(): ?\DateTimeInterface
    {
        return $this->date_rappel;
    }

    public function setDateRappel(\DateTimeInterface $date_rappel): self
    {
        $this->date_rappel = $date_rappel;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValide(): ?bool
    {
        return $this->valide;
    }

    public function setValide(bool $valide): self
    {
        $this->valide = $valide;

        return $this;
    }

    public function getIdSuivi(): ?Suivi
    {
        return $this->id_suivi;
    }

    public function setIdSuivi(Suivi $id_suivi): self
    {
        $this->id_suivi = $id_suivi;

        return $this;
    }

    public function getIdContact(): ?Contact
    {
        return $this->id_contact;
    }

    public function setIdContact(?Contact $id_contact): self
    {
        $this->id_contact = $id_contact;

        return $this;
    }
}
