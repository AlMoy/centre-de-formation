<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cerfa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $convention_financiere;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eval_entreprise;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lettre_demission;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eval_entretien;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $CV;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCerfa(): ?string
    {
        return $this->cerfa;
    }

    public function setCerfa(string $cerfa): self
    {
        $this->cerfa = $cerfa;

        return $this;
    }

    public function getConventionFinanciere(): ?string
    {
        return $this->convention_financiere;
    }

    public function setConventionFinanciere(?string $convention_financiere): self
    {
        $this->convention_financiere = $convention_financiere;

        return $this;
    }

    public function getEvalEntreprise(): ?string
    {
        return $this->eval_entreprise;
    }

    public function setEvalEntreprise(?string $eval_entreprise): self
    {
        $this->eval_entreprise = $eval_entreprise;

        return $this;
    }

    public function getLm(): ?string
    {
        return $this->lm;
    }

    public function setLm(?string $lm): self
    {
        $this->lm = $lm;

        return $this;
    }

    public function getLettreDemission(): ?string
    {
        return $this->lettre_demission;
    }

    public function setLettreDemission(?string $lettre_demission): self
    {
        $this->lettre_demission = $lettre_demission;

        return $this;
    }

    public function getEvalEntretien(): ?string
    {
        return $this->eval_entretien;
    }

    public function setEvalEntretien(?string $eval_entretien): self
    {
        $this->eval_entretien = $eval_entretien;

        return $this;
    }

    public function getCV(): ?string
    {
        return $this->CV;
    }

    public function setCV(?string $CV): self
    {
        $this->CV = $CV;

        return $this;
    }
}
