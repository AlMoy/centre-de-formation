<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnseignerRepository")
 */
class Enseigner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Session", inversedBy="enseigners")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_session;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_module;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formateur", inversedBy="enseigners")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_formateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdSession(): ?Session
    {
        return $this->id_session;
    }

    public function setIdSession(?Session $id_session): self
    {
        $this->id_session = $id_session;

        return $this;
    }

    public function getIdModule(): ?Module
    {
        return $this->id_module;
    }

    public function setIdModule(?Module $id_module): self
    {
        $this->id_module = $id_module;

        return $this;
    }

    public function getIdFormateur(): ?Formateur
    {
        return $this->id_formateur;
    }

    public function setIdFormateur(?Formateur $id_formateur): self
    {
        $this->id_formateur = $id_formateur;

        return $this;
    }
}
