<?php

namespace App\Form;

use App\Entity\Stagiaire;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StagiaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,['label'=>'Nom'])
            ->add('prenom',TextType::class,['label'=>'Prénom'])
            ->add('telephone',TelType::class,['label'=>'N° téléphone'])
            ->add('mail',EmailType::class,['label'=>'Adresse mail'])
            ->add('date_naissance',DateType::class,[
                'label'=>'Date de naissance',
                'widget'=>'single_text'
                ])
            ->add('lieu_naissance',TextType::class,['label'=>'Lieu de naissance'])
            ->add('nationalite',TextType::class,['label'=>'Nationalité'])
            ->add('numero_secu',IntegerType::class,['label'=>'N° sécurité social'])
            ->add('situation_familiale',TextType::class,['label'=>'Situation familiale'])
            ->add('permis',ChoiceType::class,[
                'label'=>'Permis',
                'choices'=>
                    [
                    'Oui'=>true,
                    'Non'=>false
                    ],
                'expanded'=>true,
                'multiple'=>false
                ])
            ->add('vehicule',ChoiceType::class,[
                'label'=>'Véhicule',
                'choices'=>
                    [
                    'Oui'=>true,
                    'Non'=>false
                    ],
                'expanded'=>true,
                'multiple'=>false
                ])
            ->add('travailleur_handicape',ChoiceType::class,[
                'label'=>'Travailleur handicapé',
                'choices'=>
                    [
                    'Oui'=>true,
                    'Non'=>false
                    ],
                'expanded'=>true,
                'multiple'=>false
                ])
            ->add('num_rue',TextType::class,['label'=>'N°'])
            ->add('rue',TextType::class,['label'=>'Nom'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stagiaire::class,
        ]);
    }
}
