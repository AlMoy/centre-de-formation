<?php

namespace App\Form;

use App\Entity\Entreprise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntrepriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,['label'=>'Nom','attr'=>['class'=>'form-control']])
            ->add('siret',TextType::class,['label'=>'SIRET','attr'=>['class'=>'form-control']])
            ->add('num_rue',TextType::class,['label'=>'N° rue','attr'=>['class'=>'form-control']])
            ->add('rue',TextType::class,['label'=>'Nom rue','attr'=>['class'=>'form-control']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entreprise::class,
        ]);
    }
}
