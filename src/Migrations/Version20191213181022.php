<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191213181022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE allocation (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, id_entreprise_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, portable VARCHAR(255) NOT NULL, fixe VARCHAR(255) NOT NULL, INDEX IDX_4C62E6381A867E8F (id_entreprise_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE demandeur_emploi (id INT AUTO_INCREMENT NOT NULL, ville_pole_emploi VARCHAR(255) NOT NULL, date_inscription INT NOT NULL, identifiant VARCHAR(255) NOT NULL, nom_conseiller VARCHAR(255) NOT NULL, antenne_mission_locale VARCHAR(255) NOT NULL, civis TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE demandeur_emploi_allocation (demandeur_emploi_id INT NOT NULL, allocation_id INT NOT NULL, INDEX IDX_8C6718A9DCFFC14A (demandeur_emploi_id), INDEX IDX_8C6718A99C83F4B2 (allocation_id), PRIMARY KEY(demandeur_emploi_id, allocation_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, cerfa VARCHAR(255) DEFAULT NULL, convention_financiere VARCHAR(255) DEFAULT NULL, eval_entreprise VARCHAR(255) DEFAULT NULL, lm VARCHAR(255) DEFAULT NULL, lettre_demission VARCHAR(255) DEFAULT NULL, eval_entretien VARCHAR(255) DEFAULT NULL, cv VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseigner (id INT AUTO_INCREMENT NOT NULL, id_session_id INT NOT NULL, id_module_id INT NOT NULL, id_formateur_id INT NOT NULL, INDEX IDX_663E85CDC4B56C08 (id_session_id), INDEX IDX_663E85CD2FF709B6 (id_module_id), INDEX IDX_663E85CD369CFA23 (id_formateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entreprise (id INT AUTO_INCREMENT NOT NULL, id_ville_id INT NOT NULL, nom VARCHAR(255) NOT NULL, siret VARCHAR(255) NOT NULL, num_rue VARCHAR(255) NOT NULL, rue VARCHAR(255) NOT NULL, INDEX IDX_D19FA60F7E4ECA3 (id_ville_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formateur (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, cv VARCHAR(255) NOT NULL, cv_europass VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formation (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE module (id INT AUTO_INCREMENT NOT NULL, id_formation_id INT NOT NULL, libelle VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, INDEX IDX_C24262871C15D5C (id_formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE niveau_scolaire (id INT AUTO_INCREMENT NOT NULL, derniere_classe VARCHAR(255) DEFAULT NULL, date_derniere_classe DATE DEFAULT NULL, dernier_diplome VARCHAR(255) DEFAULT NULL, date_dernier_diplome DATE DEFAULT NULL, dernier_emploi VARCHAR(255) DEFAULT NULL, date_dernier_emploi DATE DEFAULT NULL, nom_dernier_employeur VARCHAR(255) DEFAULT NULL, adresse_dernier_employeur VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rappel (id INT AUTO_INCREMENT NOT NULL, id_suivi_id INT NOT NULL, id_contact_id INT NOT NULL, date_rappel DATE NOT NULL, description LONGTEXT NOT NULL, valide TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_303A29C92F31B555 (id_suivi_id), INDEX IDX_303A29C9422BA59D (id_contact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, id_formation_id INT NOT NULL, nom VARCHAR(255) NOT NULL, date_debut DATE NOT NULL, date_fin DATE NOT NULL, INDEX IDX_D044D5D471C15D5C (id_formation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_stagiaire (session_id INT NOT NULL, stagiaire_id INT NOT NULL, INDEX IDX_C80B23B613FECDF (session_id), INDEX IDX_C80B23BBBA93DD6 (stagiaire_id), PRIMARY KEY(session_id, stagiaire_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stagiaire (id INT AUTO_INCREMENT NOT NULL, id_demandeur_emploi_id INT DEFAULT NULL, id_niveau_scolaire_id INT NOT NULL, id_statut_id INT NOT NULL, id_entreprise_id INT NOT NULL, id_ville_id INT NOT NULL, id_document_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, mail VARCHAR(255) NOT NULL, date_naissance DATE NOT NULL, lieu_naissance VARCHAR(255) NOT NULL, nationalite VARCHAR(255) NOT NULL, numero_secu VARCHAR(255) NOT NULL, situation_familiale VARCHAR(255) NOT NULL, permis TINYINT(1) NOT NULL, vehicule TINYINT(1) NOT NULL, travailleur_handicape TINYINT(1) NOT NULL, num_rue VARCHAR(255) NOT NULL, rue VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_4F62F7319A155D81 (id_demandeur_emploi_id), UNIQUE INDEX UNIQ_4F62F731407AF0B7 (id_niveau_scolaire_id), INDEX IDX_4F62F73176158423 (id_statut_id), INDEX IDX_4F62F7311A867E8F (id_entreprise_id), INDEX IDX_4F62F731F7E4ECA3 (id_ville_id), UNIQUE INDEX UNIQ_4F62F731DB2DB5C0 (id_document_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suivi (id INT AUTO_INCREMENT NOT NULL, id_contact_id INT NOT NULL, id_type_id INT NOT NULL, date_creation DATE NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_2EBCCA8F422BA59D (id_contact_id), INDEX IDX_2EBCCA8F1BD125E3 (id_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_suivi (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ville (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, code_postal VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E6381A867E8F FOREIGN KEY (id_entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE demandeur_emploi_allocation ADD CONSTRAINT FK_8C6718A9DCFFC14A FOREIGN KEY (demandeur_emploi_id) REFERENCES demandeur_emploi (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE demandeur_emploi_allocation ADD CONSTRAINT FK_8C6718A99C83F4B2 FOREIGN KEY (allocation_id) REFERENCES allocation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE enseigner ADD CONSTRAINT FK_663E85CDC4B56C08 FOREIGN KEY (id_session_id) REFERENCES session (id)');
        $this->addSql('ALTER TABLE enseigner ADD CONSTRAINT FK_663E85CD2FF709B6 FOREIGN KEY (id_module_id) REFERENCES module (id)');
        $this->addSql('ALTER TABLE enseigner ADD CONSTRAINT FK_663E85CD369CFA23 FOREIGN KEY (id_formateur_id) REFERENCES formateur (id)');
        $this->addSql('ALTER TABLE entreprise ADD CONSTRAINT FK_D19FA60F7E4ECA3 FOREIGN KEY (id_ville_id) REFERENCES ville (id)');
        $this->addSql('ALTER TABLE module ADD CONSTRAINT FK_C24262871C15D5C FOREIGN KEY (id_formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE rappel ADD CONSTRAINT FK_303A29C92F31B555 FOREIGN KEY (id_suivi_id) REFERENCES suivi (id)');
        $this->addSql('ALTER TABLE rappel ADD CONSTRAINT FK_303A29C9422BA59D FOREIGN KEY (id_contact_id) REFERENCES contact (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D471C15D5C FOREIGN KEY (id_formation_id) REFERENCES formation (id)');
        $this->addSql('ALTER TABLE session_stagiaire ADD CONSTRAINT FK_C80B23B613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_stagiaire ADD CONSTRAINT FK_C80B23BBBA93DD6 FOREIGN KEY (stagiaire_id) REFERENCES stagiaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F7319A155D81 FOREIGN KEY (id_demandeur_emploi_id) REFERENCES demandeur_emploi (id)');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F731407AF0B7 FOREIGN KEY (id_niveau_scolaire_id) REFERENCES niveau_scolaire (id)');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F73176158423 FOREIGN KEY (id_statut_id) REFERENCES statut (id)');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F7311A867E8F FOREIGN KEY (id_entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F731F7E4ECA3 FOREIGN KEY (id_ville_id) REFERENCES ville (id)');
        $this->addSql('ALTER TABLE stagiaire ADD CONSTRAINT FK_4F62F731DB2DB5C0 FOREIGN KEY (id_document_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE suivi ADD CONSTRAINT FK_2EBCCA8F422BA59D FOREIGN KEY (id_contact_id) REFERENCES contact (id)');
        $this->addSql('ALTER TABLE suivi ADD CONSTRAINT FK_2EBCCA8F1BD125E3 FOREIGN KEY (id_type_id) REFERENCES type_suivi (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demandeur_emploi_allocation DROP FOREIGN KEY FK_8C6718A99C83F4B2');
        $this->addSql('ALTER TABLE rappel DROP FOREIGN KEY FK_303A29C9422BA59D');
        $this->addSql('ALTER TABLE suivi DROP FOREIGN KEY FK_2EBCCA8F422BA59D');
        $this->addSql('ALTER TABLE demandeur_emploi_allocation DROP FOREIGN KEY FK_8C6718A9DCFFC14A');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F7319A155D81');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F731DB2DB5C0');
        $this->addSql('ALTER TABLE contact DROP FOREIGN KEY FK_4C62E6381A867E8F');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F7311A867E8F');
        $this->addSql('ALTER TABLE enseigner DROP FOREIGN KEY FK_663E85CD369CFA23');
        $this->addSql('ALTER TABLE module DROP FOREIGN KEY FK_C24262871C15D5C');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D471C15D5C');
        $this->addSql('ALTER TABLE enseigner DROP FOREIGN KEY FK_663E85CD2FF709B6');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F731407AF0B7');
        $this->addSql('ALTER TABLE enseigner DROP FOREIGN KEY FK_663E85CDC4B56C08');
        $this->addSql('ALTER TABLE session_stagiaire DROP FOREIGN KEY FK_C80B23B613FECDF');
        $this->addSql('ALTER TABLE session_stagiaire DROP FOREIGN KEY FK_C80B23BBBA93DD6');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F73176158423');
        $this->addSql('ALTER TABLE rappel DROP FOREIGN KEY FK_303A29C92F31B555');
        $this->addSql('ALTER TABLE suivi DROP FOREIGN KEY FK_2EBCCA8F1BD125E3');
        $this->addSql('ALTER TABLE entreprise DROP FOREIGN KEY FK_D19FA60F7E4ECA3');
        $this->addSql('ALTER TABLE stagiaire DROP FOREIGN KEY FK_4F62F731F7E4ECA3');
        $this->addSql('DROP TABLE allocation');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE demandeur_emploi');
        $this->addSql('DROP TABLE demandeur_emploi_allocation');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE enseigner');
        $this->addSql('DROP TABLE entreprise');
        $this->addSql('DROP TABLE formateur');
        $this->addSql('DROP TABLE formation');
        $this->addSql('DROP TABLE module');
        $this->addSql('DROP TABLE niveau_scolaire');
        $this->addSql('DROP TABLE rappel');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE session_stagiaire');
        $this->addSql('DROP TABLE stagiaire');
        $this->addSql('DROP TABLE statut');
        $this->addSql('DROP TABLE suivi');
        $this->addSql('DROP TABLE type_suivi');
        $this->addSql('DROP TABLE ville');
    }
}
