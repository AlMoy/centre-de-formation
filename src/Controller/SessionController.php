<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Enseigner;
use App\Form\SessionType;
use App\Repository\SessionRepository;
use App\Repository\FormationRepository;
use App\Repository\FormateurRepository;
use App\Repository\StagiaireRepository;
use App\Repository\ModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/session")
 */
class SessionController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="session_index")
     */
    public function index(SessionRepository $sessionRepository,int $page=1): Response
        {/*Calcul le nombre de pages par le nombre total de sessions divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($sessionRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 sessions, et selon la page
        $sessions=$sessionRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('session/index.html.twig', [
            'controller_name'=>'session',
            'sessions'=>$sessions,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="session_new")
     */
    public function new(Request $request,FormationRepository $formationRepository,FormateurRepository $formateurRepository,ModuleRepository $moduleRepository): Response
        {
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);
        //Si il y a une formation de passer par le formulaire et qui a un attribut id
        $formation=($request->get('formation')&&$request->get('formation')['id']) ? $formationRepository->find($request->get('formation')['id']) : null;
        //Si  la formation existe en base de données et que pour la session, la date de début soit inférieur à la date de fin
        if($form->isSubmitted()&&$form->isValid()&&!is_null($formation)&&$session->getDateDebut()<$session->getDateFin()) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            //Ajout de la formation à l'objet Session
            $session->setIdFormation($formation);
            //Parcour de la liste Enseigner
            for($i=0;$request->get("enseigner_".$i);$i++)
                {//Récupération des valeurs pour un objet Enseigner
                $dataEnseigner=$request->get("enseigner_".$i);
                //Création d'un objet Enseigner et persistance de celui
                $enseigner=new Enseigner();
                $enseigner->setIdSession($session);
                $enseigner->setIdModule($moduleRepository->find($dataEnseigner['module']));
                $enseigner->setIdFormateur($formateurRepository->find($dataEnseigner['formateur']));
                $entityManager->persist($enseigner);
                }
            //Persistance de la session et sauvegarde en base de données
            $entityManager->persist($session);
            $entityManager->flush();
            //Redirection vers cette session
            return $this->redirectToRoute('session_show', ['id' => $session->getId()]);
            }

        return $this->render('session/new.html.twig', [
            'controller_name'=>'session',
            'session' => $session,
            'formations'=>$formationRepository->findAll(),
            'formateurs'=>$formateurRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/show/{id}", name="session_show", methods={"GET"})
     */
    public function show(Session $session): Response
        {
        return $this->render('session/show.html.twig', [
            'controller_name'=>'session',
            'session' => $session,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="session_edit")
     */
    public function edit(Request $request, Session $session,FormationRepository $formationRepository,FormateurRepository $formateurRepository): Response
        {
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);
        //Si pour la session, la date de début est inférieur à la date de fin
        if ($form->isSubmitted()&&$form->isValid()&&$session->getDateDebut()<$session->getDateFin()) 
            {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('session_show', ['id' => $session->getId()]);
            }   

        return $this->render('session/edit.html.twig', [
            'controller_name'=>'session',
            'session' => $session,
            'formations'=>$formationRepository->findAll(),
            'formateurs'=>$formateurRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/delete/{id}", name="session_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Session $session): Response
        {
        if ($this->isCsrfTokenValid('delete'.$session->getId(), $request->request->get('_token')))
            {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
            }

        return $this->redirectToRoute('session_index');
        }
    }