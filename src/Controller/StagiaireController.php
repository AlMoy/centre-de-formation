<?php

namespace App\Controller;

use App\Entity\Stagiaire;
use App\Entity\NiveauScolaire;
use App\Entity\DemandeurEmploi;
use App\Entity\Document;
use App\Entity\Session;
use App\Entity\Allocation;
use App\Form\StagiaireType;
use App\Repository\StagiaireRepository;
use App\Repository\VilleRepository;
use App\Repository\AllocationRepository;
use App\Repository\StatutRepository;
use App\Repository\SessionRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\DemandeurEmploiRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/stagiaire")
 */
class StagiaireController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="stagiaire_index")
     */
    public function index(StagiaireRepository $stagiaireRepository,int $page=1): Response
        {/*Calcul le nombre de pages par le nombre total de stagiaires divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($stagiaireRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 stagiaires, et selon la page
        $stagiaires=$stagiaireRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('stagiaire/index.html.twig', [
            'controller_name'=>'stagiaire',
            'stagiaires'=>$stagiaires,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="stagiaire_new")
     */
    public function new(Request $request,VilleRepository $villeRepository,AllocationRepository $allocationRepository,StatutRepository $statutRepository,SessionRepository $sessionRepository,EntrepriseRepository $entrepriseRepository): Response
        {
        $stagiaire=new Stagiaire();
        $form=$this->createForm(StagiaireType::class, $stagiaire);
        $form->handleRequest($request);
        //Vérification de la saisi pour l'entreprise, le status, la ville, des données pour le niveau scolaire et de l'envoie des documents
        if($form->isSubmitted()&&$form->isValid()&&
            $request->get("entreprise")!==null&&
            $request->get("statut")!==null&&
            $request->get("ville")!==null&&
            $request->get("session_0")!==null&&
            $request->get("niveau_scolaire")["derniere_classe_nom"]!==null&&
            $request->get("niveau_scolaire")["derniere_classe_date"]!==null&&
            $request->get("niveau_scolaire")["dernier_diplome_nom"]!==null&&
            $request->get("niveau_scolaire")["dernier_diplome_date"]!==null&&
            $request->get("niveau_scolaire")["dernier_emploi_nom"]!==null&&
            $request->get("niveau_scolaire")["dernier_emploi_date"]!==null&&
            $request->get("niveau_scolaire")["dernier_emploi_nom_employeur"]!==null&&
            $request->get("niveau_scolaire")["dernier_emploi_adresse_employeur"]!==null&&
            $request->files->get('document')['cerfa']!==null&&
            $request->files->get('document')['convention_financiere']!==null&&
            $request->files->get('document')['lm']!==null&&
            $request->files->get('document')['cv']!==null&&
            $request->files->get('document')['lettre_demission']!==null&&
            $request->files->get('document')['eval_entreprise']!==null&&   
            $request->files->get('document')['eval_entretien']!==null)
            {
            $entityManager=$this->getDoctrine()->getManager();
            //Création et persistance d'un objet NiveauScolaire 
            $niveauScolaire=new NiveauScolaire();
            $niveauScolaire->setDerniereClasse($request->get("niveau_scolaire")["derniere_classe_nom"]);
            $niveauScolaire->setDateDerniereClasse(new \DateTime($request->get("niveau_scolaire")["derniere_classe_date"]));
            $niveauScolaire->setDernierDiplome($request->get("niveau_scolaire")["dernier_diplome_nom"]);
            $niveauScolaire->setDateDernierDiplome(new \DateTime($request->get("niveau_scolaire")["dernier_diplome_date"]));
            $niveauScolaire->setDernierEmploi($request->get("niveau_scolaire")["dernier_emploi_nom"]);
            $niveauScolaire->setDateDernierEmploi(new \DateTime($request->get("niveau_scolaire")["dernier_emploi_date"]));
            $niveauScolaire->setNomDernierEmployeur($request->get("niveau_scolaire")["dernier_emploi_nom_employeur"]);
            $niveauScolaire->setAdresseDernierEmployeur($request->get("niveau_scolaire")["dernier_emploi_adresse_employeur"]);
            $entityManager->persist($niveauScolaire);
            //Liaison des sessions au stagiaite
            for($i=0;$request->get("session_".$i);$i++)
                $stagiaire->addSession($sessionRepository->find($request->get("session_".$i)));
            //Si il y a des données pour demandeurEmpli=oi
            if(isset($request->get("demandeur_emploi")["date_inscription"])&&
                isset($request->get("demandeur_emploi")["identifiant"])&&
                isset($request->get("demandeur_emploi")["ville_pole_emploi"])&&
                isset($request->get("demandeur_emploi")["antenne_mission_local"])&&
                isset($request->get("demandeur_emploi")["nom_conseiller"])&&
                isset($request->get("demandeur_emploi")["civis"]))
                {//Création d'un objet demandeurEmploi
                $demandeurEmploi=new DemandeurEmploi();
                $demandeurEmploi->setDateInscription($request->get("demandeur_emploi")["date_inscription"]);
                $demandeurEmploi->setIdentifiant($request->get("demandeur_emploi")["identifiant"]);
                $demandeurEmploi->setVillePoleEmploi($request->get("demandeur_emploi")["ville_pole_emploi"]);
                $demandeurEmploi->setAntenneMissionLocale($request->get("demandeur_emploi")["antenne_mission_local"]);
                $demandeurEmploi->setNomConseiller($request->get("demandeur_emploi")["nom_conseiller"]);
                $demandeurEmploi->setCIVIS($request->get("demandeur_emploi")["civis"]);
                //Liaison des allocations à l'objet demandeurEmploi du stagiaire
                for($i=0;$request->get("allocation_".$i);$i++)
                    $demandeurEmploi->addIdAllocation($allocationRepository->find($request->get("allocation_".$i)));
                //Persistance de l'objet demandeurEmploi et ajout au stagiaire
                $entityManager->persist($demandeurEmploi);
                $stagiaire->setIdDemandeurEmploi($demandeurEmploi);
                }
            //Récupération des fichiers
            $document=$request->files->get('document');
            $cerfa=$document['cerfa'];
            $convention_financiere=$document['convention_financiere'];
            $lm=$document['lm'];
            $cv=$document['cv'];
            $lettre_demission=$document['lettre_demission'];
            $eval_entreprise=$document['eval_entreprise'];
            $eval_entretien=$document['eval_entretien'];
            //Si tous les fichiers sont des PDF
            if($cerfa->guessExtension()=="pdf"&&
                $convention_financiere->guessExtension()=="pdf"&&
                $lm->guessExtension()=="pdf"&&
                $cv->guessExtension()=="pdf"&&
                $lettre_demission->guessExtension()=="pdf"&&
                $eval_entreprise->guessExtension()=="pdf"&&
                $eval_entretien->guessExtension()=="pdf")
                {//Ajout de l'extension PDF
                $cerfaNom="cerfa.".$cerfa->guessExtension();
                $conventionFinanciereNom="conventionFinanciere.".$convention_financiere->guessExtension();
                $lmNom="lm.".$lm->guessExtension();
                $cvNom="cv.".$cv->guessExtension();
                $lettreDemissionNom="lettreDemission.".$lettre_demission->guessExtension();
                $evalEntrepriseNom="evalEntreprise.".$eval_entreprise->guessExtension();
                $evalEntretienNom="evalEntretien.".$eval_entretien->guessExtension();
                //Création du chemin vers le nouveau répertoire
                $chemin=$this->getParameter('documents_directory')."/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom();
                //Déplacement des fichiers
                try
                    {
                    $cerfa->move($chemin,$cerfaNom);
                    $convention_financiere->move($chemin,$conventionFinanciereNom);
                    $lm->move($chemin,$lmNom);
                    $cv->move($chemin,$cvNom);
                    $lettre_demission->move($chemin,$lettreDemissionNom);
                    $eval_entreprise->move($chemin,$evalEntrepriseNom);
                    $eval_entretien->move($chemin,$evalEntretienNom);
                    }
                catch(FileException $e)
                    {
                    echo "<pre>";
                    var_dump($e);
                    echo "</pre>";
                    exit();
                    }
                //Création du chemin pour la base de données
                $chemin="uploads/documents/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom();
                $document=new Document();
                $document->setCerfa($chemin."/".$cerfaNom);
                $document->setConventionFinanciere($chemin."/".$conventionFinanciereNom);
                $document->setLm($chemin."/".$lmNom);
                $document->setCV($chemin."/".$cvNom);
                $document->setLettreDemission($chemin."/".$lettreDemissionNom);
                $document->setEvalEntreprise($chemin."/".$evalEntrepriseNom);
                $document->setEvalEntretien($chemin."/".$evalEntretienNom);
                $entityManager->persist($document);
                }
            //Ajout des dernières données au stagiaire, et persistance de ce dernier
            $stagiaire->setIdDocument($document);
            $stagiaire->setIdNiveauScolaire($niveauScolaire);
            $stagiaire->setIdStatut($statutRepository->find($request->get("statut")));
            $stagiaire->setIdEntreprise($entrepriseRepository->find($request->get("entreprise")));
            $stagiaire->setIdVille($villeRepository->find($request->get("ville")));
            $entityManager->persist($stagiaire);
            //Sauvegarde en base de données
            $entityManager->flush();
            return $this->redirectToRoute('stagiaire_show',['id'=>$stagiaire->getId()]);
            }

        return $this->render('stagiaire/new.html.twig', [
            'controller_name'=>'stagiaire',
            'stagiaire'=>$stagiaire,
            'villes'=>$villeRepository->findAll(),
            'allocations'=>$allocationRepository->findAll(),
            'statuts'=>$statutRepository->findAll(),
            'sessions'=>$sessionRepository->findAll(),
            'entreprises'=>$entrepriseRepository->findAll(),
            'form'=>$form->createView(),
            ]);
        }

    /**
     * @Route("/show/{id}", name="stagiaire_show")
     */
    public function show(Stagiaire $stagiaire): Response
        {
        return $this->render('stagiaire/show.html.twig', [
            'controller_name'=>'stagiaire',
            'stagiaire' => $stagiaire,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="stagiaire_edit")
     */
    public function edit(Stagiaire $stagiaire,Request $request,SessionRepository $sessionRepository,VilleRepository $villeRepository,AllocationRepository $allocationRepository,StatutRepository $statutRepository,EntrepriseRepository $entrepriseRepository): Response
        {
        $form=$this->createForm(StagiaireType::class, $stagiaire);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid()) 
            {
            $entityManager=$this->getDoctrine()->getManager();
            //Si les données concernant le niveau scolaire a été envoyé
            if(isset($request->get("niveau_scolaire")["derniere_classe_nom"]))
                $stagiaire->getIdNiveauScolaire()->setDerniereClasse($request->get("niveau_scolaire")["derniere_classe_nom"]);
            if(isset($request->get("niveau_scolaire")["derniere_classe_date"]))
                $stagiaire->getIdNiveauScolaire()->setDateDerniereClasse(new \DateTime($request->get("niveau_scolaire")["derniere_classe_date"]));
            if(isset($request->get("niveau_scolaire")["dernier_diplome_nom"]))
                $stagiaire->getIdNiveauScolaire()->setDernierDiplome($request->get("niveau_scolaire")["dernier_diplome_nom"]);
            if(isset($request->get("niveau_scolaire")["dernier_diplome_date"]))
                $stagiaire->getIdNiveauScolaire()->setDateDernierDiplome(new \DateTime($request->get("niveau_scolaire")["dernier_diplome_date"]));
            if(isset($request->get("niveau_scolaire")["dernier_emploi_nom"]))
                $stagiaire->getIdNiveauScolaire()->setDernierEmploi($request->get("niveau_scolaire")["dernier_emploi_nom"]);
            if(isset($request->get("niveau_scolaire")["dernier_emploi_date"]))
                $stagiaire->getIdNiveauScolaire()->setDateDernierEmploi(new \DateTime($request->get("niveau_scolaire")["dernier_emploi_date"]));
            if(isset($request->get("niveau_scolaire")["dernier_emploi_nom_employeur"]))
                $stagiaire->getIdNiveauScolaire()->setNomDernierEmployeur($request->get("niveau_scolaire")["dernier_emploi_nom_employeur"]);
            if(isset($request->get("niveau_scolaire")["dernier_emploi_adresse_employeur"]))
                $stagiaire->getIdNiveauScolaire()->setAdresseDernierEmployeur($request->get("niveau_scolaire")["dernier_emploi_adresse_employeur"]);

            //Ajout d'un objet demandeurEmploi
            if(!$stagiaire->getIdDemandeurEmploi()&&
                isset($request->get("demandeur_emploi")["date_inscription"])&&
                isset($request->get("demandeur_emploi")["identifiant"])&&
                isset($request->get("demandeur_emploi")["ville_pole_emploi"])&&
                isset($request->get("demandeur_emploi")["antenne_mission_local"])&&
                isset($request->get("demandeur_emploi")["nom_conseiller"])&&
                isset($request->get("demandeur_emploi")["civis"]))
                {
                $demandeurEmploi=new DemandeurEmploi();
                $demandeurEmploi->setDateInscription($request->get("demandeur_emploi")["date_inscription"]);
                $demandeurEmploi->setIdentifiant($request->get("demandeur_emploi")["identifiant"]);
                $demandeurEmploi->setVillePoleEmploi($request->get("demandeur_emploi")["ville_pole_emploi"]);
                $demandeurEmploi->setAntenneMissionLocale($request->get("demandeur_emploi")["antenne_mission_local"]);
                $demandeurEmploi->setNomConseiller($request->get("demandeur_emploi")["nom_conseiller"]);
                $demandeurEmploi->setCIVIS($request->get("demandeur_emploi")["civis"]);
                //Liaison des allocations à l'objet demandeurEmploi du stagiaire
                for($i=0;$request->get("allocation_".$i);$i++)
                    $demandeurEmploi->addIdAllocation($allocationRepository->find($request->get("allocation_".$i)));
                //Persistance de l'objet demandeurEmploi et ajout au stagiaire
                $entityManager->persist($demandeurEmploi);
                $stagiaire->setIdDemandeurEmploi($demandeurEmploi);
                }
            //Ajout de l'objet demandeurEmploi du stagiaire
            else 
                {
                if(isset($request->get("demandeur_emploi")["date_inscription"]))
                    $stagiaire->getIdDemandeurEmploi()->setDateInscription($request->get("demandeur_emploi")["date_inscription"]);
                if(isset($request->get("demandeur_emploi")["identifiant"]))
                    $stagiaire->getIdDemandeurEmploi()->setIdentifiant($request->get("demandeur_emploi")["identifiant"]);
                if(isset($request->get("demandeur_emploi")["ville_pole_emploi"]))
                    $stagiaire->getIdDemandeurEmploi()->setVillePoleEmploi($request->get("demandeur_emploi")["ville_pole_emploi"]);
                if(isset($request->get("demandeur_emploi")["antenne_mission_local"]))
                    $stagiaire->getIdDemandeurEmploi()->setAntenneMissionLocale($request->get("demandeur_emploi")["antenne_mission_local"]);
                if(isset($request->get("demandeur_emploi")["nom_conseiller"]))
                    $stagiaire->getIdDemandeurEmploi()->setNomConseiller($request->get("demandeur_emploi")["nom_conseiller"]);
                if(isset($request->get("demandeur_emploi")["civis"]))
                    $stagiaire->getIdDemandeurEmploi()->setCIVIS($request->get("demandeur_emploi")["civis"]);
                }

            //Modification du statut
            if($request->get("statut")!==null)
                $stagiaire->setIdStatut($statutRepository->find($request->get("statut")));
            //Modification de l'entrepirse
            if($request->get("entreprise")!==null)
                $stagiaire->setIdEntreprise($entrepriseRepository->find($request->get("entreprise")));
            //Modification de la ville
            if($request->get("ville")!==null)
                $stagiaire->setIdVille($villeRepository->find($request->get("ville")));

            //Si des documents ont été récupéré
            if($request->files->get('document')!==null)
                {
                $pathMoveFile=$this->getParameter('documents_directory')."/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom();
                $pathSetter="uploads/documents/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom();
                //Récupération des fichiers
                $document=$request->files->get('document');
                //Parcours des types de documents
                foreach(array('cerfa','convention_financiere','lm','cv','lettre_demission','eval_entreprise','eval_entretien') as $typeDocument)
                    {//Si le type de document existe et est un PDF
                    if($document[$typeDocument]!==null&&$document[$typeDocument]->guessExtension()=="pdf")
                        {//Récupération du fichier et définition de son nom avec l'extension
                        $fichier=$document[$typeDocument];
                        $nomFichier=$typeDocument.$fichier->guessExtension();
                        //Définition du chemin stocké en base de données
                        $cheminBDD="uploads/documents/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom()."/".$nomFichier;
                        //Déplacement du fichier
                        try
                            {
                            $fichier->move($this->getParameter('documents_directory')."/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom(),$nomFichier);
                            }
                        catch(FileException $e)
                            {
                            echo "<pre>";
                            var_dump($e);
                            echo "</pre>";
                            exit();
                            }
                        //Sauvegarde du chemin dans l'objet document du stagiare
                        switch($typeDocument)
                            {
                            case 'cerfa':
                                $stagiaire->getIdDocument()->setCerfa($cheminBDD);
                                break;
                            case 'convention_financiere':
                                $stagiaire->getIdDocument()->setConventionFinanciere($cheminBDD);
                                break;
                            case 'lm':
                                $stagiaire->getIdDocument()->setLm($cheminBDD);
                                break;
                            case 'cv':
                                $stagiaire->getIdDocument()->setCv($cheminBDD);
                                break;
                            case 'lettre_demission':
                                $stagiaire->getIdDocument()->setLettreDemission($cheminBDD);
                                break;
                            case 'eval_entreprise':
                                $stagiaire->getIdDocument()->setEvalEntreprise($cheminBDD);
                                break;
                            case 'eval_entretien':
                                $stagiaire->getIdDocument()->setEvalEntretien($cheminBDD);
                                break;
                            default:
                                break;
                            }
                        }
                    }
                }
            //Sauvegarde en base de données
            $entityManager->flush();
            return $this->redirectToRoute('stagiaire_show',['id' => $stagiaire->getId()]);
            }

        return $this->render('stagiaire/edit.html.twig',[
            'controller_name'=>'stagiaire',
            'stagiaire' => $stagiaire,
            'villes'=>$villeRepository->findAll(),
            'allocations'=>$allocationRepository->findAll(),
            'statuts'=>$statutRepository->findAll(),
            'sessions'=>$sessionRepository->findAll(),
            'entreprises'=>$entrepriseRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/delete/{id}", name="stagiaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Stagiaire $stagiaire): Response
        {
        if($this->isCsrfTokenValid('delete'.$stagiaire->getId(),$request->request->get('_token'))) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            //Suppresion de son répertoire et de ses fichiers
            $document=$stagiaire->getIdDocument();
            unlink($document->getCerfa());
            unlink($document->getConventionFinanciere());
            unlink($document->getEvalEntreprise());
            unlink($document->getLm());
            unlink($document->getLettreDemission());
            unlink($document->getEvalEntretien());
            unlink($document->getCV());
            rmdir($this->getParameter('documents_directory')."/stagiaires/".$stagiaire->getPrenom()." ".$stagiaire->getNom());
            //Suppression du stagiaire en base de données
            $entityManager->remove($stagiaire);
            $entityManager->flush();
            }

        return $this->redirectToRoute('stagiaire_index');
        }
   
    /**
     * @Route("/session/add/{stagiaire}/{session}", name="add_session")
     */
    public function addSession(Stagiaire $stagiaire,Session $session): Response
        {//Liason du stagiaire à une session
        $stagiaire->addSession($session);
        $this->getDoctrine()->getManager()->flush();
        //Création de la liste des sessions
        $affichageSession='';
        foreach($stagiaire->getSessions() as $session) 
            {
            $affichageSession.='<div class="d-flex align-items-center mt-1">';
            $affichageSession.='<button type="button" class="btn btn-danger mr-1" onclick="suppressionSession('.$session->getId().')">Supprimer</button>';
            $affichageSession.='<a href="'.$this->generateUrl('session_show',['id'=>$session->getId()]).'" target="_blank" class="text-body">'.$session->getNom().'</a>';
            $affichageSession.='</div>';
            }
        return new Response($affichageSession);
        }

    /**
     * @Route("/session/delete/{stagiaire}/{session}", name="delete_session")
     */
    public function deleteSession(Stagiaire $stagiaire,Session $session): Response
        {//Suppression de la liason du stagiaire à une session
        $stagiaire->removeSession($session);
        $this->getDoctrine()->getManager()->flush();
        //Création de la liste des sessions
        $affichageSession='';
        foreach($stagiaire->getSessions() as $session) 
            {
            $affichageSession.='<div class="d-flex align-items-center mt-1">';
            $affichageSession.='<button type="button" class="btn btn-danger mr-1" onclick="suppressionSession('.$session->getId().')">Supprimer</button>';
            $affichageSession.='<a href="'.$this->generateUrl('session_show',['id'=>$session->getId()]).'" target="_blank" class="text-body">'.$session->getNom().'</a>';
            $affichageSession.='</div>';
            }
        return new Response($affichageSession);
        }

    /**
     * @Route("/allocation/add/{stagiaire}/{allocation}", name="add_allocation")
     */
    public function addAllocation(Stagiaire $stagiaire,Allocation $allocation): Response
        {
        $affichageAllocation='';
        if($stagiaire->getIdDemandeurEmploi()!=null)
            {//Récupération de l'objet DemandeurEmploie
            $demandeurEmploi=$stagiaire->getIdDemandeurEmploi();
            //Liaison de l'objet Allocation à DemandeurEmploie
            $demandeurEmploi->addIdAllocation($allocation);
            $this->getDoctrine()->getManager()->flush();
            //Création de la liste des allocations
            foreach($demandeurEmploi->getIdAllocation() as $allocation) 
                {
                $affichageAllocation.='<div class="d-flex align-items-center mt-1">';
                $affichageAllocation.='<button type="button" class="btn btn-danger mr-1" onclick="suppressionAllocation('.$allocation->getId().')">Supprimer</button>';
                $affichageAllocation.='<div>';
                $affichageAllocation.='<strong class="text-white m-0">'.$allocation->getLibelle().'</strong>';
                $affichageAllocation.='<p class="text-light m-0">'.$allocation->getDescription().'</p>';
                $affichageAllocation.='</div>';
                $affichageAllocation.='</div>';
                }
            }
        return new Response($affichageAllocation);
        }

    /**
     * @Route("/allocation/delete/{stagiaire}/{allocation}", name="delete_allocation")
     */
    public function deleteAllocation(Stagiaire $stagiaire,Allocation $allocation): Response
        {
        $affichageAllocation='';
        if($stagiaire->getIdDemandeurEmploi()!=null)
            {//Récupération de l'objet DemandeurEmploie
            $demandeurEmploi=$stagiaire->getIdDemandeurEmploi();
            //Suppression de la liaison de l'objet Allocation à DemandeurEmploie
            $stagiaire->getIdDemandeurEmploi()->removeIdAllocation($allocation);
            $this->getDoctrine()->getManager()->flush();
            //Création de la liste des allocations
            foreach($demandeurEmploi->getIdAllocation() as $allocation) 
                {
                $affichageAllocation.='<div class="d-flex align-items-center mt-1">';
                $affichageAllocation.='<button type="button" class="btn btn-danger mr-1" onclick="suppressionAllocation('.$allocation->getId().')">Supprimer</button>';
                $affichageAllocation.='<div>';
                $affichageAllocation.='<strong class="text-white m-0">'.$allocation->getLibelle().'</strong>';
                $affichageAllocation.='<p class="text-light m-0">'.$allocation->getDescription().'</p>';
                $affichageAllocation.='</div>';
                $affichageAllocation.='</div>';
                }
            }
        return new Response($affichageAllocation);
        }
    }
