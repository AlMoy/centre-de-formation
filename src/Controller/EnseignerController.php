<?php

namespace App\Controller;

use App\Entity\Enseigner;
use App\Entity\Formation;
use App\Entity\Module;
use App\Entity\Formateur;
use App\Entity\Session;
use App\Form\EnseignerType;
use App\Repository\EnseignerRepository;
use App\Repository\FormationRepository;
use App\Repository\FormateurRepository;
use App\Repository\SessionRepository;
use App\Repository\ModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/enseigner")
 */
class EnseignerController extends AbstractController
    {
    /**
     * @Route("/new/{session}", name="enseigner_new")
     */
    public function new(Request $request,ModuleRepository $moduleRepository,FormateurRepository $formateurRepository,Session $session): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        if($request->get("module")&&$request->get("formateur"))
            {//Création d'un objet Enseigner
            $enseigner=new Enseigner();
            $enseigner->setIdSession($session);
            $enseigner->setIdModule($moduleRepository->find($request->get("module")));
            $enseigner->setIdFormateur($formateurRepository->find($request->get("formateur")));
            //Persistance et sauvegarde en base de données
            $entityManager->persist($enseigner);
            $entityManager->flush();
            }
        //Renvoie une réponse avec la liste des modules de la session en html
        $affichageEnseigner="";
        foreach($session->getEnseigners() as $enseigner) 
            {
            $affichageEnseigner.='<div class="d-flex align-items-center mt-1">';
            $affichageEnseigner.='<button type="button" class="btn btn-danger" onclick="suppressionEnseigner('.$enseigner->getId().')">Supprimer</button>';
            $affichageEnseigner.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionEnseigner('.$enseigner->getId().')" data-toggle="modal" data-target="#modalEnseigner">Editer</button>';
            $affichageEnseigner.='<p class="text-light m-0">'.$enseigner->getIdModule()->getLibelle().'</p>';
            $affichageEnseigner.='</div>';
            }
        return new Response($affichageEnseigner);
        }

    /**
     * @Route("/show/{id}", name="enseigner_show", methods={"GET","POST"})
     */
    public function show(Enseigner $enseigner): Response
        {
        return $this->render('enseigner/show.html.twig', [
            'enseigner' => $enseigner,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="enseigner_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Enseigner $enseigner,ModuleRepository $moduleRepository,FormateurRepository $formateurRepository): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        //Si le module et/ou formateur a été modifié et qui existe en base de données
        if($request->get("module")&&$moduleRepository->find($request->get("module")))
            $enseigner->setIdModule($moduleRepository->find($request->get("module")));
        if($request->get("formateur")&&$formateurRepository->find($request->get("formateur")))
            $enseigner->setIdFormateur($formateurRepository->find($request->get("formateur")));
        //Sauvegarde en base de données
        $entityManager->flush();
        //Renvoie une réponse avec la liste des modules de la session en html
        $affichageEnseigner="";
        foreach($enseigner->getIdSession()->getEnseigners() as $enseigner) 
            {
            $affichageEnseigner.='<div class="d-flex align-items-center mt-1">';
            $affichageEnseigner.='<button type="button" class="btn btn-danger" onclick="suppressionEnseigner('.$enseigner->getId().')">Supprimer</button>';
            $affichageEnseigner.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionEnseigner('.$enseigner->getId().')" data-toggle="modal" data-target="#modalEnseigner">Editer</button>';
            $affichageEnseigner.='<p class="text-light m-0">'.$enseigner->getIdModule()->getLibelle().'</p>';
            $affichageEnseigner.='</div>';
            }
        return new Response($affichageEnseigner);
        }

    /**
     * @Route("/delete/{id}", name="enseigner_delete", methods={"POST"})
     */
    public function delete(Request $request, Enseigner $enseigner): Response
        {//Récupération de la session
        $session=$enseigner->getIdSession();
        //Suppression l'objet Enseigner
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($enseigner);
        $entityManager->flush();
        //Renvoie une réponse avec la liste des modules de la session en html
        $affichageEnseigner="";
        foreach($session->getEnseigners() as $enseigner) 
            {
            $affichageEnseigner.='<div class="d-flex align-items-center mt-1">';
            $affichageEnseigner.='<button type="button" class="btn btn-danger" onclick="suppressionEnseigner('.$enseigner->getId().')">Supprimer</button>';
            $affichageEnseigner.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionEnseigner('.$enseigner->getId().')" data-toggle="modal" data-target="#modalEnseigner">Editer</button>';
            $affichageEnseigner.='<p class="text-light m-0">'.$enseigner->getIdModule()->getLibelle().'</p>';
            $affichageEnseigner.='</div>';
            }
        return new Response($affichageEnseigner);
        }

    /**
     * @Route("/form/{idFormation}/{idEnseigner}", name="enseigner_form", methods={"GET","POST"})
     */
    public function form(EnseignerRepository $enseignerRepository,FormationRepository $formationRepository,FormateurRepository $formateurRepository,int $idFormation,int $idEnseigner=0): Response
        {
        $formation=$formationRepository->find($idFormation);
        $modules=$formationRepository->find($idFormation)->getModules();
        if($idEnseigner)$enseigner=$enseignerRepository->find($idEnseigner);
        else $enseigner=new Enseigner();
        return $this->render('enseigner/_form.html.twig', [
            'enseigner' => $enseigner,
            'formateurs' => $formateurRepository->findAll(),
            'modules' => $modules,
            ]);
        }
    }
