<?php

namespace App\Controller;

use App\Entity\Module;
use App\Entity\Formation;
use App\Form\ModuleType;
use App\Repository\ModuleRepository;
use App\Repository\FormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/module")
 */
class ModuleController extends AbstractController
    {
    /**
     * @Route("/new/{id}", name="module_new")
     */
    public function new(Formation $formation,Request $request): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        if($request->get("libelle")&&$request->get("description"))
            {//Création du module et ajout à la formation correspondante
            $module=new Module();
            $module->setLibelle($request->get("libelle"));
            $module->setDescription($request->get("description"));
            $formation->addModule($module);
            //Sauvegarde du module en base de données
            $entityManager->persist($module);
            $entityManager->flush();
            }
        //Renvoie une réponse avec la liste des modules de la formation en html
        $affichageModules="";
        foreach($formation->getModules() as $module) 
            {
            $affichageModules.='<div class="d-flex align-items-center mt-1">';
            $affichageModules.='<button type="button" class="btn btn-danger" onclick="suppressionModule('.$module->getId().')">Supprimer</button>';
            $affichageModules.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionModule('.$module->getId().')" data-toggle="modal" data-target="#modalModule">Editer</button>';
            $affichageModules.='<p class="text-light m-0">'.$module->getLibelle().'</p>';
            $affichageModules.='</div>';
            }
        return new Response($affichageModules);
        }

    /**
     * @Route("/show/{id}", name="module_show")
     */
    public function show(Module $module)
        {
        return $this->render('module/show.html.twig', [
            'module' => $module,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="module_edit")
     */
    public function edit(Module $module,Request $request): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        //Vérification de l'envoie des données
        if($request->get("libelle"))$module->setLibelle($request->get("libelle"));
        if($request->get("description"))$module->setDescription($request->get("description"));
        //Sauvegarde en base de données
        $entityManager->flush();
        //Renvoie une réponse avec la liste des modules de la formation en html
        $affichageModules="";
        foreach($module->getIdFormation()->getModules() as $module) 
            {
            $affichageModules.='<div class="d-flex align-items-center mt-1">';
            $affichageModules.='<button type="button" class="btn btn-danger" onclick="suppressionModule('.$module->getId().')">Supprimer</button>';
            $affichageModules.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionModule('.$module->getId().')" data-toggle="modal" data-target="#modalModule">Editer</button>';
            $affichageModules.='<p class="text-light m-0">'.$module->getLibelle().'</p>';
            $affichageModules.='</div>';
            }
        return new Response($affichageModules);
        }

    /**
     * @Route("/delete/{id}", name="module_delete")
     */
    public function delete(Request $request, Module $module): Response
        {
        $entityManager = $this->getDoctrine()->getManager();
        //Récupération de la formation
        $formation=$module->getIdFormation();
        //Suppression du module de la formation
        $module->getIdFormation()->removeModule($module);
        //Suppresion en base de données
        $entityManager->remove($module);
        $entityManager->flush();
        //Renvoie une réponse avec la liste des modules de la formation en html
        $affichageModules="";
        foreach($formation->getModules() as $module)
            {
            $affichageModules.='<div class="d-flex align-items-center mt-1">';
            $affichageModules.='<button type="button" class="btn btn-danger" onclick="suppressionModule('.$module->getId().')">Supprimer</button>';
            $affichageModules.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionModule('.$module->getId().')" data-toggle="modal" data-target="#modalModule">Editer</button>';
            $affichageModules.='<p class="text-light m-0">'.$module->getLibelle().'</p>';
            $affichageModules.='</div>';
            }
        return new Response($affichageModules);
        }

    /**
     * @Route("/form/{id}", name="module_form")
     */
    public function form(Request $request,ModuleRepository $moduleRepository,int $id=0)
        {
        $module=new Module();
        //Si l'id correspond à un module
        if($moduleRepository->find($id)!=null)
            {
            $module=$moduleRepository->find($id);
            $form=$this->createForm(ModuleType::class,$module);
            $form->handleRequest($request);
            }
        else $form = $this->createForm(ModuleType::class,$module);

        return $this->render('module/_form.html.twig', [
            'module' => $module,
            'form' => $form->createView(),
            ]);
        }
    }
