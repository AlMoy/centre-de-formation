<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Entity\Entreprise;
use App\Form\ContactType;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact")
 */
class ContactController extends AbstractController
    {
    /**
     * @Route("/new/{id}", name="contact_new")
     */
    public function new(Entreprise $entreprise,Request $request): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        if($request->get("nom")&&$request->get("prenom")&&$request->get("mail")&&$request->get("portable")&&$request->get("fixe"))
            {//Création et persistance d'un contact
            $contact=new Contact();
            $contact->setNom($request->get("nom"));
            $contact->setPrenom($request->get("prenom"));
            $contact->setMail($request->get("mail"));
            $contact->setPortable($request->get("portable"));
            $contact->setFixe($request->get("fixe"));
            $entityManager->persist($contact);
            //Ajout du contact à l'entreprise
            $entreprise->addContact($contact);
            //Sauvegarde en base de données
            $entityManager->flush();
            }
        //Renvoie une réponse avec la liste des contacts de l'entreprise en html
        $affichageContact="";
        foreach($entreprise->getContacts() as $contact) 
            {
            $affichageContact.='<div class="d-flex align-items-center mt-1">';
            $affichageContact.='<button type="button" class="btn btn-danger" onclick="suppressionContact('.$contact->getId().')">Supprimer</button>';
            $affichageContact.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionContact('.$contact->getId().')" data-toggle="modal" data-target="#modalContact">Editer</button>';
            $affichageContact.='<p class="text-light m-0">'.$contact->getPrenom().' '.$contact->getNom().'</p>';
            $affichageContact.='</div>';
            }
        return new Response($affichageContact);
        }

    /**
     * @Route("/show/{id}", name="contact_show")
     */
    public function show(Contact $contact): Response
        {
        return $this->render('contact/show.html.twig', [
            'contact' => $contact,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="contact_edit")
     */
    public function edit(Contact $contact,Request $request): Response
        {
        $entityManager=$this->getDoctrine()->getManager();
        //Vérification de l'envoie des données
        if($request->get("nom"))$contact->setNom($request->get("nom"));
        if($request->get("prenom"))$contact->setPrenom($request->get("prenom"));
        if($request->get("mail"))$contact->setMail($request->get("mail"));
        if($request->get("portable"))$contact->setPortable($request->get("portable"));
        if($request->get("fixe"))$contact->setFixe($request->get("fixe"));
        //Sauvegarde en base de données
        $entityManager->flush();
        //Renvoie une réponse avec la liste des contacts de l'entreprise en html
        $affichageContact="";
        foreach($contact->getIdEntreprise()->getContacts() as $contact) 
            {
            $affichageContact.='<div class="d-flex align-items-center mt-1">';
            $affichageContact.='<button type="button" class="btn btn-danger" onclick="suppressionContact('.$contact->getId().')">Supprimer</button>';
            $affichageContact.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionContact('.$contact->getId().')" data-toggle="modal" data-target="#modalContact">Editer</button>';
            $affichageContact.='<p class="text-light m-0">'.$contact->getPrenom().' '.$contact->getNom().'</p>';
            $affichageContact.='</div>';
            }
        return new Response($affichageContact);
        }

    /**
     * @Route("/delete/{id}", name="contact_delete")
     */
    public function delete(Contact $contact): Response
        {//Récupération de l'entreprise
        $entreprise=$contact->getIdEntreprise();
        //Suppresion du contact de la liste des contacts
        $entreprise->removeContact($contact);
        //Suppression du contact en base de données
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($contact);
        $entityManager->flush();
        //Renvoie une réponse avec la liste des contacts de l'entreprise en html
        $affichageContact="";
        foreach($entreprise->getContacts() as $contact) 
            {
            $affichageContact.='<div class="d-flex align-items-center mt-1">';
            $affichageContact.='<button type="button" class="btn btn-danger" onclick="suppressionContact('.$contact->getId().')">Supprimer</button>';
            $affichageContact.='<button type="button" class="btn btn-warning ml-1 mr-1" data-toggle="modal" onclick="editionContact('.$contact->getId().')" data-toggle="modal" data-target="#modalContact">Editer</button>';
            $affichageContact.='<p class="text-light m-0">'.$contact->getPrenom().' '.$contact->getNom().'</p>';
            $affichageContact.='</div>';
            }
        return new Response($affichageContact);
        }

    /**
     * @Route("/form/{id}", name="contact_form")
     */
    public function form(Request $request,ContactRepository $contactRepository,int $id=0): Response
        {
        $contact=new Contact();
        if($id)
            {
            $contact=$contactRepository->find($id);
            $form = $this->createForm(ContactType::class,$contact);
            $form->handleRequest($request);
            }
        else $form = $this->createForm(ContactType::class,$contact);

        return $this->render('contact/_form.html.twig', [
            'contact' => $contact,
            'form' => $form->createView(),
            ]);
        }
    }
