<?php

namespace App\Controller;

use App\Entity\Suivi;
use App\Entity\Rappel;
use App\Form\SuiviType;
use App\Repository\SuiviRepository;
use App\Repository\ContactRepository;
use App\Repository\TypeSuiviRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/suivi")
 */
class SuiviController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="suivi_index")
     */
    public function index(SuiviRepository $suiviRepository,int $page=1): Response
        {/*Calcul le nombre de pages par le nombre total de sessions divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($suiviRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 sessions, et selon la page
        $suivis=$suiviRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('suivi/index.html.twig', [
            'controller_name'=>'suivi',
            'suivis'=>$suivis,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="suivi_new", methods={"GET","POST"})
     */
    public function new(Request $request,ContactRepository $contactRepository,TypeSuiviRepository $typeSuiviRepository): Response
        {
        $suivi = new Suivi();
        $form = $this->createForm(SuiviType::class, $suivi);
        $form->handleRequest($request);
        //Vérification si un contact et un type de suivi à été selectionné
        if($form->isSubmitted()&&$form->isValid()&&isset($request->get("contact")['id'])&&isset($request->get("type_suivi")['id'])) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            //Ajout des données au suivi
            $suivi->setIdContact($contactRepository->find($request->get("contact")['id']));
            $suivi->setDateCreation(new \DateTime());
            $suivi->setIdType($typeSuiviRepository->find($request->get("type_suivi")['id']));
            //Vérification de si il y a eu des données pour un rappel
            if(isset($request->get("rappel")["date_rappel"])&&isset($request->get("rappel")["description"])&&isset($request->get("rappel")["valide"]))
                {//Création et persistance du rappel
                $rappel=new Rappel();
                $rappel->setDateRappel(new \DateTime($request->get("rappel")["date_rappel"]));
                $rappel->setDescription($request->get("rappel")["description"]);
                $rappel->setValide((bool)$request->get("rappel")["valide"]);
                $rappel->setIdSuivi($suivi);
                $rappel->setIdContact($contactRepository->find($request->get("contact")['id']));
                $entityManager->persist($rappel);
                }
            //Sauvegarde du suivi en base de données
            $entityManager->persist($suivi);
            $entityManager->flush();
            //Redirection vers le détail du suivi
            return $this->redirectToRoute('suivi_show',['id' => $suivi->getId()]);
            }

        return $this->render('suivi/new.html.twig', [
            'controller_name'=>'suivi',
            'suivi' => $suivi,
            'contacts'=>$contactRepository->findAll(),
            'typeSuivis'=>$typeSuiviRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/{id}", name="suivi_show", methods={"GET"})
     */
    public function show(Suivi $suivi): Response
        {
        return $this->render('suivi/show.html.twig', [
            'controller_name'=>'suivi',
            'suivi' => $suivi,
            ]);
        }

    /**
     * @Route("/{id}/edit", name="suivi_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Suivi $suivi,ContactRepository $contactRepository,TypeSuiviRepository $typeSuiviRepository): Response
        {
        $form = $this->createForm(SuiviType::class, $suivi);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
            {//Modification des données du suivi
            if(isset($request->get("contact")['id']))$suivi->setIdContact($contactRepository->find($request->get("contact")['id']));
            if(isset($request->get("type_suivi")['id']))$suivi->setIdType($typeSuiviRepository->find($request->get("type_suivi")['id']));
            //Si le suivi n'a pas de rappel et qu'il y a un envoie des données pour un rappel
            if(isset($request->get("rappel")["date_rappel"])&&isset($request->get("rappel")["description"])&&isset($request->get("rappel")["valide"])&&!$suivi->getRappel())
                {//Création d'un rappel et persistance en base de données
                $rappel=new Rappel();
                $rappel->setDateRappel(new \DateTime($request->get("rappel")["date_rappel"]));
                $rappel->setDescription($request->get("rappel")["description"]);
                $rappel->setValide((bool)$request->get("rappel")["valide"]);
                $rappel->setIdSuivi($suivi);
                $rappel->setIdContact($contactRepository->find($request->get("contact")['id']));
                $this->getDoctrine()->getManager()->persist($rappel);
                }
            else
                {//Sinon, modification du rappel actuelle
                if(isset($request->get("rappel")["date_rappel"]))$suivi->getRappel()->setDateRappel(new \DateTime($request->get("rappel")["date_rappel"]));
                if(isset($request->get("rappel")["description"]))$suivi->getRappel()->setDescription($request->get("rappel")["description"]);
                if(isset($request->get("rappel")["valide"]))$suivi->getRappel()->setValide((bool)$request->get("rappel")["valide"]);
                }
            //Sauvegarde en base de données
            $this->getDoctrine()->getManager()->flush();
            //Redirection vers le détail du suivi
            return $this->redirectToRoute('suivi_show', ['id' => $suivi->getId()]);
            }

        return $this->render('suivi/edit.html.twig', [
            'controller_name'=>'suivi',
            'suivi' => $suivi,
            'contacts'=>$contactRepository->findAll(),
            'typeSuivis'=>$typeSuiviRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/{id}", name="suivi_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Suivi $suivi): Response
        {
        if($this->isCsrfTokenValid('delete'.$suivi->getId(), $request->request->get('_token')))
            {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($suivi);
            $entityManager->flush();
            }

        return $this->redirectToRoute('suivi_index');
        }

    /**
     * @Route("/rappel/{id}", name="suivi_delete_rappel", methods={"POST"})
     */
    public function deleteRappel(Request $request, Suivi $suivi): Response
        {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($suivi->getRappel());
        $entityManager->flush();

        return $this->redirectToRoute('suivi_index');
        }
    }
