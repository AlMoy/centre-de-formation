<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Contact;
use App\Form\EntrepriseType;
use App\Repository\EntrepriseRepository;
use App\Repository\VilleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/entreprise")
 */
class EntrepriseController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="entreprise_index", methods={"GET|POST"})
     */
    public function index(EntrepriseRepository $entrepriseRepository,int $page=1): Response
        {/*Calcul le nombre de pages par le nombre total de entreprises divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($entrepriseRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 entreprises, et selon la page
        $entreprises=$entrepriseRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('entreprise/index.html.twig', [
            'controller_name'=>'entreprise',
            'entreprises'=>$entreprises,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="entreprise_new", methods={"GET","POST"})
     */
    public function new(Request $request,VilleRepository $villeRepository): Response
        {
        $entreprise = new Entreprise();
        $form = $this->createForm(EntrepriseType::class, $entreprise);
        $form->handleRequest($request);
        //Vérification qu'il y a une saisi d'une ville
        $ville=$request->get("ville") ? $villeRepository->find($request->get("ville")) : null;
        //Si il y a au moins un contact de saisi et que la ville existe en base de données
        if ($form->isSubmitted() && $form->isValid() && $request->get("contact_0") && !is_null($ville)) 
            {
            $entityManager=$this->getDoctrine()->getManager();
            //Ajout de la ville à l'objet Entreprise
            $entreprise->setIdVille($ville);
            //Parcour de la liste Contact
            for($i=0;$request->get("contact_".$i);$i++)
                {//Récupération des valeurs pour un objet Contact
                $dataContact=$request->get("contact_".$i);
                //Création d'un objet Contact et persistance de celui
                $contact=new Contact();
                $contact->setNom($dataContact["nom"]);
                $contact->setPrenom($dataContact["prenom"]);
                $contact->setMail($dataContact["mail"]);
                $contact->setPortable($dataContact["portable"]);
                $contact->setFixe($dataContact["fixe"]);
                $entityManager->persist($contact);
                //Ajout de cet objet à l'ojet Entreprise
                $entreprise->addContact($contact);
                }
            //Persistance de l'entreprise et sauvegarde en base de données
            $entityManager->persist($entreprise);
            $entityManager->flush();
            //Redirection vers cette entreprise
            return $this->redirectToRoute('entreprise_show', ['id' => $entreprise->getId()]);
            }

        return $this->render('entreprise/new.html.twig', [
            'controller_name'=>'entreprise',
            'entreprise' => $entreprise,
            'villes'=>$villeRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/show/{id}", name="entreprise_show", methods={"GET"})
     */
    public function show(Entreprise $entreprise): Response
        {
        return $this->render('entreprise/show.html.twig', [
            'controller_name'=>'entreprise',
            'entreprise' => $entreprise,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="entreprise_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Entreprise $entreprise,VilleRepository $villeRepository): Response
        {
        $form = $this->createForm(EntrepriseType::class, $entreprise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
            {//Si il y a une ville de passé par le formulaire et qui existe dans la base de données
            if($request->get("ville")&&!is_null($villeRepository->find($request->get("ville"))))
                $entreprise->setIdVille($villeRepository->find($request->get("ville")));
            //Enregistrement en base de données
            $this->getDoctrine()->getManager()->flush();
            //Redirection vers la page de l'entreprise
            return $this->redirectToRoute('entreprise_show', ['id' => $entreprise->getId()]);
            }

        return $this->render('entreprise/edit.html.twig', [
            'controller_name'=>'entreprise',
            'entreprise' => $entreprise,
            'villes'=>$villeRepository->findAll(),
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/delete/{id}", name="entreprise_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Entreprise $entreprise): Response
        {
        if ($this->isCsrfTokenValid('delete'.$entreprise->getId(), $request->request->get('_token'))) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entreprise);
            $entityManager->flush();
            }

        return $this->redirectToRoute('entreprise_index');
        }
    }
