<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CentreController extends AbstractController
	{
    /**
     * @Route("/", name="centre")
     */
    public function index()
    	{
        return $this->render('centre/index.html.twig', ['controller_name'=>'centre']);
    	}
	}
