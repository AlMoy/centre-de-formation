<?php

namespace App\Controller;

use App\Entity\Formation;
use App\Entity\Module;
use App\Form\FormationType;
use App\Repository\FormationRepository;
use App\Repository\ModuleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/formation")
 */
class FormationController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="formation_index")
     */
    public function index(FormationRepository $formationRepository,int $page=1)
        {/*Calcul le nombre de pages par le nombre total de formations divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($formationRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 formations, et selon la page
        $formations=$formationRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('formation/index.html.twig', [
            'controller_name'=>'formation',
            'formations'=>$formations,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="formation_new")
     */
    public function new(Request $request)
        {
        $formation = new Formation();
        $form = $this->createForm(FormationType::class,$formation);
        $form->handleRequest($request);
        //Si il y a au moins 1 module de saisi
        if($form->isSubmitted()&&$form->isValid()&&$request->get("module_0")) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            //Parcour des modules
            for($i=0;$request->get("module_".$i);$i++)
                {//Récupération des données du module
                $dataModule=$request->get("module_".$i);
                //Création et persistance du module
                $module=new Module();
                $module->setLibelle($dataModule["libelle"]);
                $module->setDescription($dataModule["description"]);
                $entityManager->persist($module);
                //Ajout du module à la formation
                $formation->addModule($module);
                }
            //Sauvegarde en base de données
            $entityManager->persist($formation);
            $entityManager->flush();
            //Redirection vers la page du formateur
            return $this->redirectToRoute('formation_show', ['id' => $formation->getId()]);
            }
        return $this->render('formation/new.html.twig', [
            'controller_name'=>'formation',
            'formation' => $formation,
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/show/{id}", name="formation_show")
     */
    public function show(Formation $formation)
        {
        return $this->render('formation/show.html.twig', [
            'controller_name'=>'formation',
            'formation' => $formation,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="formation_edit")
     */
    public function edit(Request $request, Formation $formation)
        {
        $form = $this->createForm(FormationType::class, $formation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
            {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('formation_show', ['id' => $formation->getId()]);
            }

        return $this->render('formation/edit.html.twig', [
            'controller_name'=>'formation',
            'formation' => $formation,
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/delete/{id}", name="formation_delete")
     */
    public function delete(Request $request, Formation $formation)
        {
        if ($this->isCsrfTokenValid('delete'.$formation->getId(), $request->request->get('_token'))) 
            {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($formation);
            $entityManager->flush();
            }

        return $this->redirectToRoute('formation_index');
        }
    }
