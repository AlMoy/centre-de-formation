<?php

namespace App\Controller;

use App\Entity\Formateur;
use App\Form\FormateurType;
use App\Repository\FormateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/formateur")
 */
class FormateurController extends AbstractController
    {
    /**
     * @Route("/index/{page}", name="formateur_index")
     */
    public function index(FormateurRepository $formateurRepository,int $page=1)
        {/*Calcul le nombre de pages par le nombre total de formateurs divisé par 10
        Cette variable doit rester en float pour le template twig correspondant*/
        $pages=count($formateurRepository->findAll())/10;
        //Vérifie que la valeur de l'argument $page est bien compris entre 1 et $pages
        $page=($page>=1&&$page<=(int)$pages)?$page:1;
        //Récupére jusqu'a 10 formateurs, et selon la page
        $formateurs=$formateurRepository->findBy([],null,10,($page*10)-10);
        /*Si il y a au moins 4 pages qui précède la page actuelle
        Alors la pagination commence avec le numéro de la page actuelle - 4
        Sinon, elle commence à la page 1*/
        $pageDebut=($page-4>=1)?$page-4:1;
        /*Si il y a plus de 4 pages qui suit la page actuelle
        Alors la pagination s'arrête avec le numéro de la page actuelle + 4
        Sinon, elle s'arrête à la dernière page*/
        $pageFin=($page+4<=(int)$pages)?$page+4:(int)$pages;
        return $this->render('formateur/index.html.twig', [
            'controller_name'=>'formateur',
            'formateurs'=>$formateurs,
            'pageDebut'=>$pageDebut,
            'pageFin'=>$pageFin,
            'page'=>$page,
            'pages'=>$pages
            ]);
        }

    /**
     * @Route("/new", name="formateur_new")
     */
    public function new(Request $request)
        {
        $formateur=new Formateur();
        $form=$this->createForm(FormateurType::class,$formateur);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
            {//Récupération des CV
            $cv=$form['cv']->getData();
            $cvEuropass=$form['cv_europass']->getData();
            //Si les fichiers sont en PDF
            if($cv->guessExtension()=="pdf"&&$cvEuropass->guessExtension()=="pdf")
                {//Ajout de l'extension PDF aux fichiers et définition du dossier du nouveau formateur
                $cvNom="cv.".$cv->guessExtension();
                $cvEuropassNom="cvEuropass.".$cvEuropass->guessExtension();
                $dossierFormateur="/formateurs/".$formateur->getPrenom()." ".$formateur->getNom();
                try
                    {//Récupération du chemin vers les documents du formateur
                    $chemin=$this->getParameter('documents_directory').$dossierFormateur;
                    //Déplacement des CV
                    $cv->move($chemin,$cvNom);
                    $cvEuropass->move($chemin,$cvEuropassNom);
                    }
                catch(FileException $e)
                    {
                    echo "<pre>";
                    var_dump($e);
                    echo "</pre>";
                    exit();
                    }
                //Enregistrement des chemins vers ces CV
                $chemin="uploads/documents".$dossierFormateur;
                $formateur->setCv($chemin."/".$cvNom);
                $formateur->setCvEuropass($chemin."/".$cvEuropassNom);
                //Sauvegarde en base de données
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($formateur);
                $entityManager->flush();
                //Redirection vers la page du formateur
                return $this->redirectToRoute('formateur_show',['id'=>$formateur->getId()]);
                }
            }
        return $this->render('formateur/new.html.twig', [
            'controller_name'=>'formateur',
            'formateur'=>$formateur,
            'form'=>$form->createView(),
            ]);
        }

    /**
     * @Route("/show/{id}", name="formateur_show")
     */
    public function show(Formateur $formateur)
        {
        return $this->render('formateur/show.html.twig', [
            'controller_name'=>'formateur',
            'formateur'=>$formateur,
            ]);
        }

    /**
     * @Route("/edit/{id}", name="formateur_edit")
     */
    public function edit(Request $request, Formateur $formateur)
        {
        $form=$this->createForm(FormateurType::class,$formateur);
        $form->handleRequest($request);

        if($form->isSubmitted()&&$form->isValid())
            {//Récupération des CV et du chemin du dossier du formateur
            $cv=$form['cv']->getData();
            $cvEuropass=$form['cv_europass']->getData();
            $dossierFormateur="/formateurs/".$formateur->getPrenom()." ".$formateur->getNom();
            //Si le fichier CV a été reçu et est un pdf
            if($cv&&$cv->guessExtension()=="pdf")
                try
                    {//Déplacement du CV
                    $cv->move($this->getParameter('documents_directory').$dossierFormateur,"cv.".$cv->guessExtension());
                    }
                catch(FileException $e)
                    {
                    echo "<pre>";
                    var_dump($e);
                    echo "</pre>";
                    exit();
                    }

            //Si le fichier CV Europass a été reçu et est un pdf
            if($cvEuropass&&$cvEuropass->guessExtension()=="pdf")
                try
                    {//Déplacement du CV Europass
                    $cvEuropass->move($this->getParameter('documents_directory').$dossierFormateur,"cvEuropass.".$cvEuropass->guessExtension());
                    }
                catch(FileException $e)
                    {
                    echo "<pre>";
                    var_dump($e);
                    echo "</pre>";
                    exit();
                    }
            //Sauvegarde en base de données
            $this->getDoctrine()->getManager()->flush();
            //Redirection vers la page du formateur
            return $this->redirectToRoute('formateur_show', ['id' => $formateur->getId()]);
            }

        return $this->render('formateur/edit.html.twig', [
            'controller_name'=>'formateur',
            'formateur' => $formateur,
            'form' => $form->createView(),
            ]);
        }

    /**
     * @Route("/delete/{id}", name="formateur_delete")
     */
    public function delete(Request $request, Formateur $formateur)
        {
        if($this->isCsrfTokenValid('delete'.$formateur->getId(), $request->request->get('_token')))
            {
            $entityManager=$this->getDoctrine()->getManager();
            //Suppresion de son répertoire et de ses CV
            $dossierFormateur=$this->getParameter('documents_directory')."/formateurs/".$formateur->getPrenom()." ".$formateur->getNom();
            unlink($dossierFormateur."/cv.pdf");
            unlink($dossierFormateur."/cvEuropass.pdf");
            rmdir($dossierFormateur);
            //Suppression du formateur en base de données
            $entityManager->remove($formateur);
            $entityManager->flush();
            }
        return $this->redirectToRoute('formateur_index');
        }
    }
