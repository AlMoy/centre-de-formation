-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 06 jan. 2020 à 18:26
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `centreformation`
--

--
-- Déchargement des données de la table `allocation`
--

INSERT INTO `allocation` (`id`, `libelle`, `description`) VALUES
(1, 'Allocation 1', 'Description allocation 1'),
(2, 'Allocation 2', 'Description allocation 2'),
(3, 'Allocation 3', 'Description allocation 3'),
(4, 'Allocation 4', 'Description allocation 4'),
(5, 'Allocation 5', 'Description allocation 5'),
(6, 'Allocation 6', 'Description allocation 6');

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `id_entreprise_id`, `nom`, `prenom`, `mail`, `portable`, `fixe`) VALUES
(1, 1, 'Merveille', 'Julie', 'julie.merveille@gmail.com', '0675003355', '0275003355'),
(4, 1, 'Merch', 'Cho', 'cho.merch@gmail.com', '063353254', '023353254'),
(5, 1, 'Noel', 'Franck', 'franck.noel@gmail.com', '0656892500', '0256892500');

--
-- Déchargement des données de la table `demandeur_emploi`
--

INSERT INTO `demandeur_emploi` (`id`, `ville_pole_emploi`, `date_inscription`, `identifiant`, `nom_conseiller`, `antenne_mission_locale`, `civis`) VALUES
(1, 'Redon', 13, 'AlMoy', 'Josepin', 'test', 0);

--
-- Déchargement des données de la table `demandeur_emploi_allocation`
--

INSERT INTO `demandeur_emploi_allocation` (`demandeur_emploi_id`, `allocation_id`) VALUES
(1, 1),
(1, 3);

--
-- Déchargement des données de la table `document`
--

INSERT INTO `document` (`id`, `cerfa`, `convention_financiere`, `eval_entreprise`, `lm`, `lettre_demission`, `eval_entretien`, `cv`) VALUES
(1, 'uploads/documents/stagiaires/Alex Moysan/cerfa.pdf', 'uploads/documents/stagiaires/Alex Moysan/conventionFinanciere.pdf', 'uploads/documents/stagiaires/Alex Moysan/evalEntreprise.pdf', 'uploads/documents/stagiaires/Alex Moysan/lm.pdf', 'uploads/documents/stagiaires/Alex Moysan/lettreDemission.pdf', 'uploads/documents/stagiaires/Alex Moysan/evalEntretien.pdf', 'uploads/documents/stagiaires/Alex Moysan/cv.pdf');

--
-- Déchargement des données de la table `enseigner`
--

INSERT INTO `enseigner` (`id`, `id_session_id`, `id_module_id`, `id_formateur_id`) VALUES
(1, 1, 1, 2),
(2, 1, 2, 3),
(3, 1, 4, 4),
(4, 1, 5, 2),
(17, 3, 6, 2),
(18, 3, 8, 4);

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id`, `id_ville_id`, `nom`, `siret`, `num_rue`, `rue`) VALUES
(1, 6, 'Ib formation', '333 928 190 00045', '1', 'PLACE DE LA PYRAMIDE TOUR ATLANTIQUE - LA DÉFENSE 9');

--
-- Déchargement des données de la table `formateur`
--

INSERT INTO `formateur` (`id`, `nom`, `prenom`, `telephone`, `mail`, `adresse`, `cv`, `cv_europass`) VALUES
(2, 'Michel', 'Jean', '0669359812', 'jean.michel@gmail.com', 'addr postal', 'uploads/documents/formateurs/Jean Michel/cv.pdf', 'uploads/documents/formateurs/Jean Michel/cvEuropass.pdf'),
(3, 'Janvier', 'Jacques', '0669359812', 'jacques.janvier@gmail.com', 'addr postal', 'uploads/documents/formateurs/Jacques Janvier/cv.pdf', 'uploads/documents/formateurs/Jacques Janvier/cvEuropass.pdf'),
(4, 'Perraine', 'Paul', '0669359812', 'paul.perraine@gmail.com', 'addr postal', 'uploads/documents/formateurs/Paul Perraine/cv.pdf', 'uploads/documents/formateurs/Paul Perraine/cvEuropass.pdf'),
(5, 'Zoudine', 'Patrick', '0669359812', 'patrick.zoudine@gmail.com', 'addr postal', 'uploads/documents/formateurs/Patrick Zoudine/cv.pdf', 'uploads/documents/formateurs/Patrick Zoudine/cvEuropass.pdf');

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`id`, `libelle`) VALUES
(1, 'Développeur C#'),
(2, 'Développeur PHP'),
(3, 'Développeur Angular');

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20191202172108', '2019-12-03 17:37:52');

--
-- Déchargement des données de la table `module`
--

INSERT INTO `module` (`id`, `id_formation_id`, `libelle`, `description`) VALUES
(1, 1, 'Langage C#', 'Les bases du C#'),
(2, 1, 'Framework .Net', 'Utilisation du framework .Net'),
(3, 1, 'SQL', 'Utilisation du langage SQL avec la base de données MariaDB'),
(4, 1, 'ASP .Net', 'Création des projets web'),
(5, 1, 'UWP', 'Création de logicielle windows'),
(6, 2, 'Langage PHP', 'Les bases de PHP  et l\'objet en PHP'),
(7, 2, 'SQL', 'Utilisation du langage SQL avec la base de données MariaDB'),
(8, 2, 'Symfony 4', 'Apprentissage du framework Symfony, dans la 4eme version majeure'),
(9, 1, 'JQuery', 'Utilisation de la bibliothèque javascript JQuery'),
(10, 1, 'Bootstrap', 'Utilisation de la bibliothèque CSS Bootstrap'),
(11, 2, 'JQuery', 'Utilisation de la bibliothèque javascript JQuery'),
(12, 2, 'Bootstrap', 'Utilisation de la bibliothèque CSS Bootstrap'),
(13, 3, 'HTML', 'Apprentissage du langage HTML'),
(14, 3, 'CSS', 'Apprentissage du langage CSS'),
(15, 3, 'JavaScript', 'Apprentissage du langage HTML'),
(16, 3, 'Angular', 'Apprentissage du framework javascript Angular');

--
-- Déchargement des données de la table `niveau_scolaire`
--

INSERT INTO `niveau_scolaire` (`id`, `derniere_classe`, `date_derniere_classe`, `dernier_diplome`, `date_dernier_diplome`, `dernier_emploi`, `date_dernier_emploi`, `nom_dernier_employeur`, `adresse_dernier_employeur`) VALUES
(1, 'PEOC .Net', '2019-12-17', 'SIO', '2019-02-14', 'Stagiaire', '2019-12-26', 'Merveille', '35 rue Nantes, Renac 36660');

--
-- Déchargement des données de la table `rappel`
--

INSERT INTO `rappel` (`id`, `id_suivi_id`, `id_contact_id`, `date_rappel`, `description`, `valide`) VALUES
(3, 2, 5, '2019-12-31', 'Description rappel', 0);

--
-- Déchargement des données de la table `session`
--

INSERT INTO `session` (`id`, `id_formation_id`, `nom`, `date_debut`, `date_fin`) VALUES
(1, 1, 'Développeur .Net (décembre)', '2019-12-02', '2019-12-13'),
(3, 2, 'Développeur Symfony (janvier)', '2020-01-01', '2020-01-18');

--
-- Déchargement des données de la table `session_stagiaire`
--

INSERT INTO `session_stagiaire` (`session_id`, `stagiaire_id`) VALUES
(1, 1),
(3, 1);

--
-- Déchargement des données de la table `stagiaire`
--

INSERT INTO `stagiaire` (`id`, `id_demandeur_emploi_id`, `id_niveau_scolaire_id`, `id_statut_id`, `id_entreprise_id`, `id_ville_id`, `id_document_id`, `nom`, `prenom`, `telephone`, `mail`, `date_naissance`, `lieu_naissance`, `nationalite`, `numero_secu`, `situation_familiale`, `permis`, `vehicule`, `travailleur_handicape`, `num_rue`, `rue`) VALUES
(1, 1, 1, 1, 1, 1, 1, 'Moysan', 'Alex', '0789498984', 'moysan.alex@gmail.com', '1997-01-13', 'Rennes', 'Francais', '98498494', 'Célibataire', 0, 0, 0, '35', 'Nantes');

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id`, `libelle`) VALUES
(1, 'Pré-inscrit'),
(2, 'Inscrit'),
(3, 'Apprenti'),
(4, 'Stagiaire'),
(5, 'Diplômé'),
(6, 'Non diplômé'),
(7, 'Abandon'),
(8, 'Démission');

--
-- Déchargement des données de la table `suivi`
--

INSERT INTO `suivi` (`id`, `id_contact_id`, `id_type_id`, `date_creation`, `description`) VALUES
(1, 1, 1, '2019-12-13', 'Description du suivi'),
(2, 4, 5, '2019-12-13', 'Description suivi');

--
-- Déchargement des données de la table `type_suivi`
--

INSERT INTO `type_suivi` (`id`, `libelle`) VALUES
(1, 'Type 1'),
(2, 'Type 2'),
(3, 'Type 3'),
(4, 'Type 4'),
(5, 'Type 5'),
(6, 'Type 6');

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id`, `nom`, `code_postal`) VALUES
(1, 'Redon', '35600'),
(2, 'Rennes', '35000'),
(3, 'Brest', '29200'),
(4, 'Vannes', '56000'),
(5, 'Toulouse', '31000'),
(6, 'Paris', '75000'),
(7, 'Lyon', '69001'),
(8, 'Lille', '59000'),
(9, 'Valenciennes', '59300'),
(10, 'Marseille', '13000'),
(11, 'Nice', '06000');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
